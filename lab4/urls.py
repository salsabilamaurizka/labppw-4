from django.urls import path, include
from . import views
#path for app
urlpatterns = [
    path('', views.home, name='home'),
    path('experience', views.experience, name='experience'),
    path('education', views.education, name='education'),
    path('skill', views.skill, name='skill'),
    path('contact', views.contact, name='contact'),
    path('guestbooks', views.guestbooks, name='guestbooks'),
    path('schedules', views.schedules, name='schedules'),
    path('add_schedules', views.add_schedules, name='add_schedules'),
    path('remove_all', views.remove_all, name='remove_all'),
]
