from django import forms
from .models import Schedules


class DateInput(forms.DateInput):
    input_type = 'date'

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedules
        fields = ['activity', 'date', 'place', 'category']
        widgets = {
            'date': DateInput()}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
