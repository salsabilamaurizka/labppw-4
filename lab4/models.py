from django.db import models
from django.utils import timezone
from datetime import datetime, date
# Create your models here.

class Schedules (models.Model):
    #hari,tanggal, jam, nama kegiatannya, tempat, kategori
    date = models.DateTimeField()
    activity = models.CharField(max_length = 40)
    place = models.CharField(max_length = 20)
    category = models.CharField(max_length = 20)

    def __str__(self):
        return self.activity
