from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Activities (models.Model):
    #hari,tanggal, jam, nama kegiatannya, tempat, kategori
    act_date = models.DateTimeField()
    act_name = models.CharField(max_length = 40)
    place = models.CharField(max_length = 20)
    category = models.ChaField(max_length = 20)
