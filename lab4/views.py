from django.shortcuts import render, redirect
from .models import Schedules
from . import forms
from django.http import HttpResponseRedirect

# Create your views here.
def home(request):
    return render(request, 'home.html')

def experience(request):
    return render(request, 'experience.html')

def education(request):
    return render(request, 'education.html')

def skill(request):
    return render(request, 'skill.html')

def contact(request):
    return render(request, 'contact.html')

def guestbooks(request):
    return render(request, 'guestbooks.html')

def schedules(request):
    schedule = Schedules.objects.all()
    return render(request, 'schedules.html', {'schedules':schedule})

def add_schedules(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/schedules')
    else:
        form = forms.ScheduleForm()
    return render(request,'add_schedules.html',{'form':form})

def remove_all(request):
    remove_all = Schedules.objects.all().delete()
    return HttpResponseRedirect('/schedules')
